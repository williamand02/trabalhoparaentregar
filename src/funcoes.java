import java.util.Scanner;

public class funcoes {
	static Scanner keyboard = new Scanner( System.in );
	public static int lerResponder(String frase) {
		System.out.println(frase);
		String input = keyboard.nextLine();
		return Integer.parseInt(input);
	}
	public static int[] lerArray(String frase,int[] numeros) {
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(frase);
			String input = keyboard.nextLine();
			numeros[i] = Integer.parseInt(input);
		}
		return numeros;
	}
	public static int[] imprimiArray(int[] numeros) {
		System.out.print("Valores do vetor:[");
		for (int i = 0; i < numeros.length; i++) {
			System.out.print(numeros[i]);
			if(i+1<numeros.length) {
				System.out.print(",");
			}
		}
		System.out.println("]");
		return numeros;
	}
	public static int calcularMedia(int[] valores) {
		int numero =0;
		for (int i = 0; i < valores.length; i++) {
			numero += valores[i];
		}
		return (numero/valores.length);
		
	}

}
