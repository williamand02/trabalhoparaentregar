package n2;

import java.util.Scanner;

public class exercicio_19_deterministico {
	static Scanner keyboard = new Scanner( System.in );
	public static int lerResponder(String frase) {
		System.out.println(frase);
		String input = keyboard.nextLine();
		return Integer.parseInt(input);
	}
	  public static void main(String[] args) {
		    int valor = lerResponder("Digite o Valor pago:");
		    int produto = lerResponder("Digite o Valor do produto:");
		    System.out.println("Troco: "+(valor-produto));
		  }   

}
