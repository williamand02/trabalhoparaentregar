/*gera um caminho aleatorio marcado pelo numero 1*/
package n2;
import java.util.Random;
public class exercicio_19_probabilisticos {
	static int[][] mapa={{0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0}, {0,0,0,0,0},{0,0,0,0,0}};
	static int linha =0;
	static int coluna =0;
	
	public static void imprimirArray(int[][] m) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {
				System.out.print(mapa[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}
	public static void caminhar(int[][] m) {
		Random gerador = new Random();
		int lr=0;
		m[0][0]=1;
		while(m[m.length-1][m.length-1]==0) {
			lr= gerador.nextInt(2);
			if(lr==0 && linha!=m.length-1) {
				linha++;
			}else if(lr==1 && coluna!=m.length-1){
				coluna++;
			}else {
				
			}
			m[linha][coluna]=1;
		}
		imprimirArray(m);
	}
	public static void main(String[] args) {
		caminhar(mapa);

	}

}
