package n2;

import java.util.Scanner;

public class exercicio_17 {
	static Scanner keyboard = new Scanner(System.in);
	static int n = 0;
	static int numeroEntrada;
	static int[] numero;
	static int total = 0;

	public static void main(String[] args) {
		 lerDados("Digite um numero:"); 
		 calcular();
	}

	public static void lerDados(String frase) {
		System.out.println(frase);
		String input = keyboard.nextLine();
		n = input.length();
		numero = new int[n];
		for (int i = 0; i < numero.length; i++) {
			numero[i] = Integer.parseInt("" + input.charAt(i));
		}
		numeroEntrada = Integer.parseInt(input);
	}

	public static void calcular() {
		for (int i = 0; i < numero.length; i++) {
			System.out.print(numero[i]+"^"+n);

			if(i < numero.length-1) {
				System.out.print("+");
			}
			total = (int) (total+(Math.pow(numero[i], n)));
		}
		System.out.println("="+total+"    Numero digitado: "+numeroEntrada);
		if (total == numeroEntrada) {
			System.out.println("Este � um n�mero de Armstrong");
		} else {
			System.out.println("Este n�o � um n�mero de Armstrong");
		}
	}

}
