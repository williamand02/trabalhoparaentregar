public class exercicio_25 {
	public static void main(String[] args) {
		int a [] ={32,45,89,66,12,35,10,96,38,15,13,11,65,81,35,64,16,89,54,19};
		int b []= new int[a.length];
		int contB = 0;
		int aux;
		
		for (int i = 0; i < a.length; i++) {
			if(a[i]<50) {
				b[contB]=i;
				contB++;
			}
		}
		
		for (int i = 0; i < contB; i++) {
			aux=b[i];
			for (int j = 0; j < contB; j++) {
				if(b[i]>b[j]) {
					aux=b[i];
					b[i]=b[j];
					b[j]=aux;
				}
			}
		}
		System.out.print("Valores do vetor:[");
		for (int i = 0; i < contB; i++) {
			System.out.print(b[i]);
			if(i+1<contB) {
				System.out.print(",");
			}
		}
		System.out.println("]");
	}
}