public class exercicio_28 {
	public static void main(String[] args) {
		int[][] matriz1 = new int[4][4];
		int[][] matriz2 = new int[4][4];
		int[][] matrizSoma = new int[4][4];
		
		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1.length; j++) {
				matriz1[i][j] = i;
				matriz2[i][j] = i+j;
			}
		}

		System.out.println("Matriz 1");
		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1.length; j++) {
				System.out.print(matriz1[i][j]);
			}
			System.out.println();
		}
		System.out.println();
		
		System.out.println("Matriz 2");
		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2.length; j++) {
				System.out.print(matriz2[i][j]);
			}
			System.out.println();
		}
		System.out.println();
		
//		soma da matriz
		for (int i = 0; i < matrizSoma.length; i++) {
			for (int j = 0; j < matrizSoma.length; j++) {
				matrizSoma[i][j] =  matriz1[i][j]+matriz2[i][j];
			}
		}

		System.out.println("matrizSoma");
		for (int i = 0; i < matrizSoma.length; i++) {
			for (int j = 0; j < matrizSoma.length; j++) {
				System.out.print(matrizSoma[i][j]);
			}
			System.out.println();
		}
	}
}
