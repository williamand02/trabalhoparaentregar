import java.util.Scanner;

public class exercicio_13a {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner( System.in );
		int[] numeros = new int[10];
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Digite o "+(i+1)+"� numero:");
			String input = keyboard.nextLine();
			numeros[i] = Integer.parseInt(input);
		}
		System.out.print("Valores do vetor:[");
		for (int i = 0; i < numeros.length; i++) {
			System.out.print(numeros[i]);
			if(i+1<numeros.length) {
				System.out.print(",");
			}
		}
		System.out.println("]");
		System.out.print("Valores do vetor inverso:[");
		for (int i = 0; i < numeros.length; i++) {
			System.out.print(numeros[numeros.length-i-1]);
			if(i+1<numeros.length) {
				System.out.print(",");
			}
		}
		System.out.println("]");
	}

}
