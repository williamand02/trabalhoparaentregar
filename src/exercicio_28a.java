import java.util.Random;

public class exercicio_28a {

	public static void main(String[] args) {
		int[][] matriz1 = new int[4][4];
		int[][] matriz2 = new int[4][4];
		
        carregarMatriz(matriz1);		
        carregarMatriz(matriz2);
        
        imprimiMatriz(matriz1);
        imprimiMatriz(matriz2);
        
        somarMatriz(matriz1, matriz2);
	}

	private static void carregarMatriz(int[][] mat) {
		Random gerador = new Random();
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat.length; j++) {
				//adciona um numero aleatoria menor que 10 a matriz na posi��o [i][j]
				mat[i][j] = gerador.nextInt(10);
			}
		}
	}
	private static void imprimiMatriz(int[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat.length; j++) {
				System.out.print(mat[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	private static void somarMatriz(int[][] mat1,int[][] mat2) {
		for (int i = 0; i < mat1.length; i++) {
			for (int j = 0; j < mat1.length; j++) {
				//adciona espa�o nos numero menores que 10
				if((mat1[i][j]+mat2[i][j])<10) {
					System.out.print(" "+(mat1[i][j]+ mat2[i][j])+" ");
				}else {
					System.out.print(mat1[i][j]+ mat2[i][j]+" ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

}
