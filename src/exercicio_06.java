public class exercicio_06 {

	public static void main(String[] args) {
		int lado_a = funcoes.lerResponder("Digite o lado \"a\" do triagulo");
		int lado_b = funcoes.lerResponder("Digite o lado \"b\" do triagulo");
		int lado_c = funcoes.lerResponder("Digite o lado \"c\" do triagulo");
		
		if(lado_a==0 || lado_b==0 || lado_c==0) {
			System.out.println("Nenhum dos lados pode ser igual a zero");
		}else if(lado_a>(lado_b+lado_c)||lado_b>(lado_a+lado_c)||lado_c>(lado_a+lado_b)) {
			System.out.println("Um lado n�o pode ser maior do que a soma dos outros dois.");
		}else {
			if(lado_a==lado_b && lado_a==lado_c) {
				System.out.println("Este e um triangulo Equil�tero");
			}else if (lado_a==lado_b || lado_a==lado_c || lado_b==lado_c) {
				System.out.println("Este e um triangulo Is�celes");
			}else {
				System.out.println("Este e um triangulo Escaleno");
			}
		}
	}
}
